FROM tomcat:9.0.41-jdk15-openjdk-oracle
ADD tmchat/target/tmch*.war /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh" "run"]
